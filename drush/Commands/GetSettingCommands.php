<?php
namespace Drush\Commands;

use Drupal\Core\Site\Settings;

/**
 * Class GetConfigDirectory gets config dir.
 */
class GetSettingCommands extends DrushCommands {

    /**
     * Wrapper for getting config directory.
     *
     * @usage drush gfd
     *   Get the conFig Directory.
     *
     * @bootstrap configuration
     * @command composer-base:get-config-directory
     *
     * @aliases gfd
     */
    public function getConfigDirectory() {
        return Settings::get('config_sync_directory');
    }

    /**
     * Wrapper for Settings. Get any arbitrary setting by name.
     *
     * @param $name
     *   Setting name to get.
     * @usage drush get-setting config_sync_directory
     *   This is a bad example, because `drush gfd` already does this.
     *
     * @bootstrap configuration
     * @command composer-base:get-setting
     *
     * @aliases setting
     */
    public function getSetting($name) {
        return Settings::get($name);
    }

}