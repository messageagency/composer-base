<?php

namespace Drush\Commands;

use Drush\Exceptions\CommandFailedException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ThemeSetupCommands initializes the default theme for a new build.
 */
class ThemeSetupCommands extends DrushCommands
{

    /**
     * Perform one-time initial setup for given theme.
     *
     * @param $machine_name
     *   Name of theme to initialize.
     * @usage drush ma-base-theme:setup new_theme_example
     *   Initializes the new_theme_example theme.
     *   Assumed path is web/themes/custom/new_theme_example.
     *
     * @bootstrap full
     * @command ma-base-theme:setup
     */
    public function maBaseThemeSetup($machine_name)
    {
        // Install and set new theme as default.
        $machine_name = str_replace('-', '_', $machine_name);
        $subtheme_path = \Drupal::service('extension.list.theme')->getPath($machine_name);
        if (empty($subtheme_path) || !is_dir($subtheme_path)) {
            $this->logger()->warning('Theme "{name}" was not found.', [
                'name' => $machine_name,
            ]);
            return;
        }

        $config_factory = \Drupal::configFactory();
        $subtheme_path = \Drupal::service('extension.list.theme')->getPath($machine_name);

        // Forcibly create and update config from new theme.
        $config_paths = [
            'created' => $subtheme_path . '/config/install',
            'updated' => $subtheme_path . '/config/replace',
        ];
        foreach ($config_paths as $action => $config_path) {
            if (!is_dir($config_path)) {
                throw new CommandFailedException(dt('The directory "!directory" was not found.', ['!directory' => $config_path]));
            }
            $finder = Finder::create();
            $finder->files()->in($config_path);
            foreach ($finder as $file) {
                $config_name = substr($file->getBasename(), 0, -4);
                $file_contents = $file->getContents();
                $data = Yaml::parse($file_contents);
                $config_factory->getEditable($config_name)
                    ->setData($data)
                    ->save(TRUE);
                $this->logger()->notice('{action} "{name}".', [
                    'action' => $action,
                    'name' => $config_name,
                ]);
            }
        }
    }

    /**
     * Try to enable given theme.
     *
     * @param $machine_name
     *   Name of theme to enable.
     * @usage drush ma-base-theme:setup new_theme_example
     *   Enable new_theme_example theme and set as default.
     *
     * @bootstrap full
     * @command ma-base-theme:enable
     */
    public function maBaseThemeEnable($machine_name) {
        // Install and set new theme as default.
        $machine_name = str_replace('-', '_', $machine_name);
        $subtheme_path = \Drupal::service('extension.list.theme')->getPath($machine_name);
        if (empty($subtheme_path) || !is_dir($subtheme_path)) {
            $this->logger()->warning('Theme "{name}" was not found.', [
                'name' => $machine_name,
            ]);
            return;
        }

        $this->logger()->notice('Installed "{name}" as the site theme.', [
            'name' => $machine_name,
        ]);
        $config_factory = \Drupal::configFactory();
        $config_factory->getEditable('system.theme')
            ->set('default', $machine_name)
            ->save(TRUE);
        $themes = $config_factory->getEditable('core.extension')
            ->get('theme');
        $themes = array_merge($themes, array($machine_name => 0));
        $config_factory->getEditable('core.extension')
            ->set('theme', $themes)
            ->save(TRUE);
    }


    /**
     * Try to enable the default content module.
     *
     * @param $machine_name
     *   Name of module to enable.
     * @usage drush ma-sprout-content:enable site_name_content
     *   Enable site_name_content module.
     *
     * @bootstrap full
     * @command ma-sprout-content:enable
     */
    public function maSproutContentEnable($machine_name) {
        // Install the sprout content module for this site.
        $machine_name = str_replace('-', '_', $machine_name) . "_content";
        $subtheme_path = \Drupal::service('extension.list.module')->getPath($machine_name);
        if (empty($subtheme_path) || !is_dir($subtheme_path)) {
            $this->logger()->warning('Module "{name}" was not found.', [
                'name' => $machine_name,
            ]);
            return;
        }
        \Drupal::service('module_installer')->install([$machine_name]);
        $this->logger()->notice('Installed "{name}" module.', [
            'name' => $machine_name,
        ]);
    }

}