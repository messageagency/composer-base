<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;

class ScriptHandler
{

    protected static function getDrupalRoot($project_root)
    {
        return $project_root . '/web';
    }

    public static function createRequiredFiles(Event $event)
    {
        $fs = new Filesystem();
        $root = static::getDrupalRoot(getcwd());

        $dirs = [
            'modules',
            'profiles',
            'themes',
        ];

        // Required for unit testing
        foreach ($dirs as $dir) {
            if (!$fs->exists($root . '/' . $dir)) {
                $fs->mkdir($root . '/' . $dir);
                $fs->touch($root . '/' . $dir . '/.gitkeep');
            }
        }

        // Create the files directory with chmod 0777
        if (!$fs->exists($root . '/sites/default/files')) {
            $oldmask = umask(0);
            $fs->mkdir($root . '/sites/default/files', 0777);
            umask($oldmask);
            $event->getIO()->write("Create a sites/default/files directory with chmod 0777");
        }
    }

    // This is called by the QuickSilver deploy hook to convert from
    // a 'lean' repository to a 'fat' repository. This should only be
    // called when using this repository as a custom upstream, and
    // updating it with `terminus composer <site>.<env> update`. This
    // is not used in the GitHub PR workflow.
    public static function prepareForPantheon()
    {
        // Get rid of any .git directories that Composer may have added.
        // n.b. Ideally, there are none of these, as removing them may
        // impair Composer's ability to update them later. However, leaving
        // them in place prevents us from pushing to Pantheon.
        $dirsToDelete = [];
        $finder = new Finder();
        foreach (
            $finder
                ->directories()
                ->in(getcwd())
                ->ignoreDotFiles(false)
                ->ignoreVCS(false)
                ->depth('> 0')
                ->name('.git')
            as $dir) {
            $dirsToDelete[] = $dir;
        }
        $fs = new Filesystem();
        $fs->remove($dirsToDelete);

        // Fix up .gitignore: remove everything above the "::: cut :::" line
        $gitignoreFile = getcwd() . '/.gitignore';
        $gitignoreContents = file_get_contents($gitignoreFile);
        $gitignoreContents = preg_replace('/.*::: cut :::*/s', '', $gitignoreContents);
        file_put_contents($gitignoreFile, $gitignoreContents);
    }

    /**
     * Remove git directories to prevent creation of submodules.
     */
    public static function removeGitDirectories()
    {
        $drupalFinder = new DrupalFinder();
        $drupalFinder->locateRoot(getcwd());
        $drupalRoot = $drupalFinder->getDrupalRoot();
        exec('find ' . $drupalRoot . ' -name \'.git\' | xargs rm -rf');
    }

    /**
     * Remove dev packages so that they can be updated. Otherwise, composer chokes on missing .git directory.
     */
    public static function removeDevReleases(Event $event) {
        /** @var \Composer\Composer $composer */
        $composer = $event->getComposer();
        $repo = $composer->getRepositoryManager()->getLocalRepository();
        foreach ($composer->getPackage()->getRequires() as $name => $link) {
            if (strpos($link->getPrettyConstraint(), 'dev') === FALSE) {
                continue;
            }
            $event->getIO()->notice("Removing dev package $name so it can be updated.");
            if ($package = $repo->findPackage($name, $link->getConstraint())) {
                $path = $composer->getInstallationManager()->getInstallPath($package);
                $composer->getInstallationManager()->getInstaller($package->getType())->uninstall($repo, $package);
            }
        }
    }

    /**
     * Run available site theme npm scripts.
     */
    public static function runSiteThemeCommand(Event $event)
    {
        $drupalFinder = new DrupalFinder();
        $drupalFinder->locateRoot(getcwd());
        $drupalRoot = $drupalFinder->getDrupalRoot();
        $dir_array = [];
        foreach (new \DirectoryIterator($drupalRoot . '/themes/custom') as $index => $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                $dir_array[] = $fileinfo->getFilename();
            }
        }
        if (empty($dir_array)) {
            $event->getIO()->write('Please install the custom theme.');
            return;
        }
        elseif (count($dir_array) < 2) {
            $theme_name = $dir_array[0];
        }
        else {
            $input = $event->getIO()->select('Which theme would you like to compile [0]:', $dir_array, '0');
            $theme_name = $dir_array[$input];
        }

        chdir($drupalRoot . '/themes/custom/' . $theme_name);

        // Get list of npm scripts
        $scripts = json_decode(file_get_contents("./package.json"), true)['scripts'];
        $scripts_list = array_keys($scripts);
        array_unshift($scripts_list, 'Cancel');
        $output = [];
        $result = NULL;
        $args = $event->getArguments();
        $event->getIO()->debug(print_r($args, TRUE));
        $install = array_search('--install', $args);
        if ($install !== false) {
            unset($args[$install]);
            $event->getIO()->write('Running "npm install"');
            try {
                exec('npm install --loglevel=error', $output, $result);
            }
            catch(\Exception $e) {
                $event->getIO()->write(PHP_EOL . implode(PHP_EOL, $output));
                $event->getIO()->error($e->getMessage());
                return $result;
            }
        }
        if (!empty($args)) {
            $command = $args[0];
        }
        else {
            $input = $event->getIO()->select('Which command would you like to run [Cancel]:', $scripts_list, '0');
            $command = $scripts_list[$input];
        }

        if (!in_array($command, $scripts_list)) {
            throw new \Exception("Invalid npm script name.", -1);
        }
        elseif ($command == 'Cancel') {
            $event->getIO()->write('Cancelling...');
        }
        else {
            $event->getIO()->write('Running "npm run ' . $command . '"');
            exec('npm run ' . $command, $output, $result);
            $event->getIO()->write(PHP_EOL . implode(PHP_EOL, $output));
            return $result;
        }
    }

}
