<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all environments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to ensure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * Place the config directory outside of the Drupal root.
 */
$settings['config_sync_directory'] = dirname(DRUPAL_ROOT) . '/config';

$settings['skip_permissions_hardening'] = TRUE;

/**
 * If there is a local or environmental settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}
elseif (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
  $pantheon_settings = __DIR__ . "/settings." . $_ENV['PANTHEON_ENVIRONMENT'] . ".php";
  if (file_exists($pantheon_settings)) {
    include $pantheon_settings;
  }
}
