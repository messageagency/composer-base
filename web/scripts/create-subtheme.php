<?php

/**
 * Quick and dirty drush script to create a radix subtheme from ma kit.
 *
 * @usage
 *   drush scr web/scripts/create-subtheme --id=<id> --name=<name>
 * If using lando:
 *   cd web
 *   lando drush scr scripts/create-subtheme --id=<id> --name=<name>
 *
 * @param id
 *   The machine name of the theme to be created.
 * @param name
 *   The human readable name of the theme to be created.
 */

if (!$id = drush_get_option('id')) {
  drush_print(dt('Theme id is required'));
}

if (!$name = drush_get_option('name')) {
  drush_print(dt('Theme name is required'));
}

composer_base_create_subtheme($name, $id);

/**
 * Implements drush_hook_COMMAND().
 */
function composer_base_create_subtheme($name, $machine_name) {
  $machine_name = str_replace(' ', '_', strtolower($machine_name));
  $search = array(
    '/[^a-z0-9_]/', // Remove characters not valid in function names.
    '/^[^a-z]+/',   // Functions must begin with an alpha character.
  );
  $machine_name = preg_replace($search, '_', $machine_name);

  // Description of subtheme.
  $description = 'Custom theme.';

  // Determine the path to the new subtheme.
  $subtheme_path = 'themes';
  if ($path = drush_get_option('path')) {
    $subtheme_path = drush_trim_path($path);
  }
  $subtheme_path = drush_normalize_path(drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $subtheme_path . '/custom/' . $machine_name);

  $kit_url = 'https://packages.messageagency.com/ma-base.tgz';
  $kit_name = $kit = 'ma-base';

  // Switch to a temp directory.
  $current_dir = getcwd();
  chdir(drush_tempdir());

  drush_print(dt('Downloading @kit_name from @kit_url...', array(
    '@kit_name' => (!empty($kit_name)) ? $kit_name . ' kit' : $kit_name,
    '@kit_url' => $kit_url,
  )));
  if ($filepath = drush_download_file($kit_url)) {
    $filename = basename($filepath);

    // Decompress the zip archive.
    $files = drush_tarball_extract($filename, getcwd(), TRUE);

    // Re-index array.
    // This fixes an issue where a .tag.gz tarball returns a non-zero array.
    $files = array_values($files);
    $kit_path = getcwd() . '/' . $files[0];

    // Set working directory back to the previous working directory.
    chdir($current_dir);
  }

  if (!is_dir(dirname($subtheme_path))) {
    drush_die(dt('The directory "!directory" was not found.', array('!directory' => dirname($subtheme_path))));
  }
  drush_op('drush_copy_dir', $kit_path, $subtheme_path);

  composer_base_replace_do_replacements($subtheme_path, $machine_name, $name, $description);

  composer_base_rename_do_renames($subtheme_path, $machine_name, $kit);

  // Notify user of the newly created theme.
  $message = 'Successfully created the Radix subtheme "!name" in "!path" using the "!kit" kit';

  $message = dt($message . '.', array(
    '!name' => $name,
    '!path' => $subtheme_path,
    '!kit' => $kit,
  ));
  drush_print($message);

  composer_base_install_subtheme($subtheme_path, $machine_name);

  add_build_step($machine_name);
}

function composer_base_rename_do_renames($subtheme_path, $machine_name, $kit) {
  $finder = new Symfony\Component\Finder\Finder();
  $finder->files()->in($subtheme_path);
  foreach ($finder as $file) {
    if ($file->isDir()) {
      composer_base_rename_do_renames($file->getPath(), $machine_name, $kit);
    }
    else {
      if (strpos($file->getBasename(), $kit) !== FALSE) {
        $file_new_path = str_replace($kit, $machine_name, $file->getPathname());
        drush_op('rename', $file->getPathname(), $file_new_path);
      }
    }
  }
}

function composer_base_replace_do_replacements($subtheme_path, $machine_name, $name, $description) {
  $alterations = array(
    'RADIX_SUBTHEME_NAME' => $name,
    'RADIX_SUBTHEME_DESCRIPTION' => $description,
    'RADIX_SUBTHEME_MACHINE_NAME' => $machine_name,
    'hidden: true' => '',
  );

  $finder = new Symfony\Component\Finder\Finder();
  $finder->files()->in($subtheme_path);
  foreach ($finder as $file) {
    if ($file->isDir()) {
      composer_base_replace_do_replacements($file->getPath(), $machine_name, $name, $description);
    }
    else {
      $file_contents = $file->getContents();
      $file_contents = str_replace(array_keys($alterations), $alterations, $file_contents);
      file_put_contents($file->getPathname(), $file_contents);
    }
  }
}

function composer_base_install_subtheme($subtheme_path, $machine_name) {
  // Install and set new theme as default.
  $config_factory = \Drupal::configFactory();
  $config_factory->getEditable('system.theme')
    ->set('default', $machine_name)
    ->save(TRUE);
  $themes = $config_factory->getEditable('core.extension')
    ->get('theme');
  $themes = array_merge($themes, array($machine_name => 0));
  $config_factory->getEditable('core.extension')
    ->set('theme', $themes)
    ->save(TRUE);

  $message = dt('Installed "!name" as the site theme' . '.', [
    '!name' => $machine_name,
  ]);
  drush_print($message);

  // Install new and update existing config with config from new theme.
  $config_paths = [
    'created' => $subtheme_path . '/config/install',
    'updated' => $subtheme_path . '/config/replace',
  ];
  foreach ($config_paths as $action => $config_path) {
    if (!is_dir(dirname($config_path))) {
      drush_die(dt('The directory "!directory" was not found.', ['!directory' => dirname($config_path)]));
    }
    $finder = new Symfony\Component\Finder\Finder();
    $finder->files()->in($config_path);
    foreach ($finder as $file) {
      $config_name = substr($file->getBasename(), 0, -4);
      $file_contents = $file->getContents();
      $data = \Symfony\Component\Yaml\Yaml::parse($file_contents);
      $config_factory->getEditable($config_name)
        ->setData($data)
        ->save(TRUE);

      $message = dt('!action "!name"' . '.', [
        '!action' => $action,
        '!name' => $config_name,
      ]);
      drush_print($message);
    }
  }
}

function add_build_step($machine_name) {
  $filename = ( $_SERVER['DOCUMENT_ROOT'] . '../bitbucket-pipelines.yml' );
  $file_contents = file_get_contents($filename);

  $needles = ["      name: Deploy to Pantheon"];
  $alterations = ["      name: Build theme assets
      image: circleci/node:10.16.0
      services:
        - docker
      caches:
        - docker
      script:
        - *bash_env_export
        - *bash_env_source
        - cd web/themes/custom/$machine_name
        - npm install --loglevel=error
        - npm run production
      artifacts:
        - web/themes/custom/$machine_name/assets/**
  - step:
      name: Deploy to Pantheon"];

  $file_contents = str_replace($needles, $alterations, $file_contents);
  file_put_contents($filename, $file_contents);
}
